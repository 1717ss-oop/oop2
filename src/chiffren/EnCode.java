/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chiffren;

import java.util.List;

/**
 *
 * @author leo
 */
public interface EnCode {
	 /**
     * waehlt einen zufaelligen index aus der allSchluessel- Liste und gibt den schluesselwert zurueck 
     * @return schluesselwert
     */
    public int getRandomSchluessel();
    /**
     * encodiert eine Liste von Textzeilen mit zufaelligen Schluessel
     * @param text Liste von textzeilen 
     * @return encodierter text
     */
    public List<String> encode(List<String> text);
    /**
     * encodiert eine Zeile mit einem zufealligen schluessel
     * @param Zeile
     * @return encodierte Zeile
     */
    public String encode(String line);
    
    /**
     * encodiert eine Liste von textZeilen mit gegebenen Schluessel
     * @param text ZeilenListe
     * @param schluessel
     * @return ecodierte Liste von Textzeilen
     */
    public List<String> encode(List<String> text, int schluessel);
    /**
     * initalisiert wenn noetig den schluessel neu und die chiffrenliste
     * encodiert eine Zeile/string mit dem gegeben schluessel
     * @param line zu encodierende Zeile
     * @param schluessel 
     * @return die uebersetzte bzw. encodierte zeile
     */
    public String encode(String line, int schluessel);
}

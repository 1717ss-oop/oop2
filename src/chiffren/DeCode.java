/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chiffren;

import java.util.List;

/**
 *
 * @author leo
 */
public interface DeCode {
    
    /**
     * ermittelt durch textanalyse welcher schluessel fuer den decodierten code infrage kommt
     * @param text decodierte Text dessen Schluessel gesucht wird
     * @return schluessel welcher ermittelt wurde
     */
    public int getSchluessel(String text);
    /**
     * wenn der schluessel unbekannt ist, wird er in dieser methode zuerst ermittelt, und danach wird normal decodiert
     * @param line zu decodierende Zeile
     * @return decodierte Zeile
     */
    public String decode (String line);
    /**
     * eine Liste von textzeilen wird decodiert
     * @param text Liste der Textzeilen
     * @return Liste von decodierten Zeilen
     */
    public List<String> decode (List<String> text);
    /**
     * initalisiert wenn noetig den schluessel neu und die chiffrenListe 
     * decodiert eine Zeile mit dem gegeben schluessel und den jeweiligen �quivalenten (chiffrenliste)
     * @param line Zeile
     * @param schluessel 
     * @return decodierte Zeile
     */
    public String decode (String line, int schluessel);
    /**
     * eine Liste von textzeilen wird mit gegebenen Schluessel decodiert
     * @param text Liste von textzeilen
     * @param schluessel
     * @return decodierte Liste von Zeilen
     */
    public List<String> decode (List<String> text, int schluessel);
}

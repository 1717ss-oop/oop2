/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chiffren;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 *
 * @author leo
 */
public class Code implements EnCode,DeCode{
    
    public static Scanner scan = new Scanner(System.in);
    
    private Map<Character,Character> chiffreEn; //cache encode map here
    private Map<Character,Character> chiffreDe; //cache decode map here
    private int cEn=-1; //schluessel fuer die gecachete encode map
    private int cDe=-1; //schluessel fuer die gecachete decode map
    private int[] allSchluessel;
    private int anzahlZeichen=26;
    /**
     * erstellt ein codeobjekt, gleichzeitig wird dessen allSchluessel-Liste mit Schluesselwerten befuellt (default)
     */
    public Code(){
        int[] temp= {1,3,5,7,9,11,15,17,19,21,23,25}; //valide schluessel
        allSchluessel=temp; 
    }
    
    /**
     * waehlt einen zufaelligen index aus der allSchluessel- Liste und gibt den schluesselwert zurueck 
     * @return schluesselwert
     */
    public int getRandomSchluessel(){
        return allSchluessel[(int)Math.floor(Math.random()*allSchluessel.length)];
    }

    /**
     * ermittelt durch textanalyse welcher schluessel fuer den decodierten code infrage kommt
     * @param text decodierte Text dessen Schluessel gesucht wird
     * @return schluessel welcher ermittelt wurde
     */
    public int getSchluessel(String text){
        //zaele mithilfe der String.split() Methode (auch am letzten relevanten char soll noch gesplittet werden)
        text.concat("$");
        TreeMap<Integer,Character> results = new TreeMap<Integer,Character>();
        for (int i=97; i<97+26; i++){//zeichen von a-z jeweils zaelen (Ascii-code a=97)
            String[] temp = text.split(Character.toString((char)i));
            results.put(temp.length,(char)i);
        }
        String[] testwords = text.split(" ");
        int schluessel=0;
        char[] haeufigkeitB = {'e','n','i','s','r','a','t','d','h','u','l','c','g','m','o','b','w','f','k','z','p','v','j','y','x','q'};
        for(int i=0; i<haeufigkeitB.length; i++){
            schluessel=(((int)results.lastEntry().getValue()-96) % 26);
            /*
            bisher nur gelesenes zeichen: 5 * x = schluessel
            schluessel(neu) = x = schluessel * stellen(e)^(-1) = schluessel * 21
            */
            schluessel = (schluessel * mInvers(((int)haeufigkeitB[i])-96))%26;
            while (true){
                try{
                   System.out.println("Ist \""+decode(testwords[0],schluessel)+"\" ein sinnvolles Wort? (y|n)");
                   String next=scan.next();
                   if (next.equals("y")){
                       return schluessel;
                   }else if(next.equals("n")){
                       break;
                   }
                }catch(Exception e){
                    //shouldnt happen
                }
            }
        }
        return schluessel;
    }
    
    /**
     * errechnet das Inverse zu n
     * @param n zahl dessen inverses errechnet werden soll
     * @return Inverses
     */
    public int mInvers(int n){
        int[] temp = erweiterterEuklid(n,anzahlZeichen);
        if (temp[0]==1){
            while(temp[1]<0){temp[1]+=26;}
            return temp[1];
        }else{
            return -1;
        }
    }
    /**
     * errechnet Inverses nach euklidischen Algrithmus
     * @param a davon wird Inverses berechnet 
     * @param b z
     * @return array aus {0,a^-1,rest)
     */
   private int[] erweiterterEuklid(int a,int b){
        int[] result = {a,1,0};
        if (b == 0){return result;}
        result = erweiterterEuklid(b, a % b);
        int temp=result[2];
        result[2]=result[1]-(a / b)*result[2];	
        result[1]=temp;
        System.out.println(""+result[0]+" "+result[1]+" "+result[2]);
        return result;
   }
   
   /**
    * ersetzt alle chars bzw. buchstaben einer Zeile mit deren de-/bzw. encode-�quivalent
    * @param line Zeile/string die uebersetzt werden soll
    * @param chiffre Map in der das jeweilige �quivalent von allen Buchstaben gespeichert ist
    * @return uebersetzte Zeile
    */
    private String replacer(String line,Map<Character,Character> chiffre){
        StringBuilder str = new StringBuilder(line);
        for (int i=0; i<str.length();i++){
            str.setCharAt(i, chiffre.get(str.charAt(i)));
        }
        return str.toString();
    }
    /**
     * initalisiert wenn noetig den schluessel neu und die chiffrenliste
     * encodiert eine Zeile/string mit dem gegeben schluessel
     * @param line zu encodierende Zeile
     * @param schluessel 
     * @return die uebersetzte bzw. encodierte zeile
     */
    public String encode(String line, int schluessel){
        if (cEn!=schluessel){
            cEn = schluessel;
            chiffreEn = new HashMap<Character,Character>();
            for (int i=1; i<26; i++){
                chiffreEn.put((char)(i+96),(char)(((i*schluessel) % 26)+96));
            }
            chiffreEn.put('z', 'z');	//was ist mit m? automatisch?
            chiffreEn.put(' ',' ');
        }
        return replacer(line,chiffreEn);
    }
    
    /**
     * initalisiert wenn noetig den schluessel neu und die chiffrenListe 
     * decodiert eine Zeile mit dem gegeben schluessel und den jeweiligen �quivalenten (chiffrenliste)
     * @param line Zeile
     * @param schluessel 
     * @return decodierte Zeile
     */
    public String decode (String line, int schluessel){
        if (cDe!=schluessel){
            cDe = schluessel;
            chiffreDe = new HashMap<Character,Character>();
            for (int i=1; i<26; i++){
                chiffreDe.put((char)(i+96),(char)(((i*mInvers(schluessel)) % 26)+96));
            }
            chiffreDe.put('z', 'z');
            chiffreDe.put(' ',' ');
        }
        return replacer(line,chiffreDe);
    }
    
    /**
     * wenn der schluessel unbekannt ist, wird er in dieser methode zuerst ermittelt, und danach wird normal decodiert
     * @param line zu decodierende Zeile
     * @return decodierte Zeile
     */
    public String decode (String line){
        int schluessel = getSchluessel(line);
        return decode(line,schluessel);
    }
    
    /**
     * eine Liste von textzeilen wird decodiert
     * @param text Liste der Textzeilen
     * @return Liste von decodierten Zeilen
     */
    public List<String> decode (List<String> text){
        String strText ="";
        for (String line : text){
            strText+=line;
        } 
        int schluessel = getSchluessel(strText);
        return decode(text,schluessel);
    }
    
    /**
     * eine Liste von textzeilen wird mit gegebenen Schluessel decodiert
     * @param text Liste von textzeilen
     * @param schluessel
     * @return decodierte Liste von Zeilen
     */
    public List<String> decode (List<String> text, int schluessel){
        for (int i=0; i<text.size(); i++){
            text.set(i, decode(text.get(i),schluessel));
        }
        return text;
    }
    
    /**
     * encodiert eine Liste von Textzeilen mit zufaelligen Schluessel
     * @param text Liste von textzeilen 
     * @return encodierter text
     */
    public List<String> encode(List<String> text){
        int schluessel = getRandomSchluessel();
        return encode(text,schluessel);
    }
    /**
     * encodiert eine Zeile mit einem zufealligen schluessel
     * @param Zeile
     * @return encodierte Zeile
     */
    public String encode(String line){
        return encode(line,getRandomSchluessel());
    }
    
    /**
     * encodiert eine Liste von textZeilen mit gegebenen Schluessel
     * @param text ZeilenListe
     * @param schluessel
     * @return ecodierte Liste von Textzeilen
     */
    public List<String> encode(List<String> text, int schluessel){
        for (int i=0; i<text.size(); i++){
            text.set(i, encode(text.get(i),schluessel));
        }
        return text;
    } 
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chiffren;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Leonard Krause <kontakt@herr-ek.de>
 */
public class TextIO {
    /**
     * Liste der Umlaute und deren schreibweise (fuer fkt. replace umlaute)
     */
    private static Map<String,String> umlaute;
    
    
    /**
     * Schreibt in die gewuenschte txtdatei, alle Zeilen der Uebergabeliste
     * @param lines liste aller Zeilen, die geschrieben werden sollen
     * @param path dateipfad bzw. datei, in die geschrieben werden soll
     * @return true falls das schreiben erfolgreich war, false falls Exceptions auftraten
     */
    public static Boolean writeText(List<String> lines,String path){
        System.out.println("try write: "+path);
        try{
            BufferedWriter bw = new BufferedWriter(new FileWriter(path));
            for (String line : lines){
                bw.write(line+"\n");
            }
            bw.flush();
            bw.close();		
            return true;
        }catch (IOException e){
            System.out.println("check the path to the file!");
            System.out.println(e);
            return false;
        }finally{

        }
    }
   /**
    * Liest die uebergebende Datei zweile fuer Zeile aus. 
    * in jeder Zeiler werden zuerst ggf. vorhandene umlaute geaendert, danach wird der inhalt nur noch auf Buchstaben gekuerzt 
    * @param path gibt den DateiPfad zur Textdatei an
    * @return die Liste aller gelesenen Zeilen mitsamt deren Aenderung, oder NULL falls Exceptions auftraten
    */
    public static List<String> readText(String path){
        List<String> lines = new ArrayList<String>();
        try{
            System.out.println("try read: "+path);
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path),"UTF-8"));  //Filereader,utf-8?
            String line = br.readLine();
            Pattern p = Pattern.compile("([äöüß]){1}");
            Matcher m;
            while (line != null) {
                line=line.toLowerCase(); // nur [A-Za-z äöüß] erlaubt 
                m=p.matcher(line);
                while (m.find()){ 		//replaceAll ist in java behindert...
                    line=m.replaceFirst(replaceUmlaute(m.group()));
                    m=p.matcher(line);
                }
                line=line.replaceAll("[^a-z ]", "");                      
                lines.add(line);
                line = br.readLine();
            } 
            br.close(); 	
            return lines;
        }catch(Exception a){
            System.out.println("check the path to the folder!");
            System.out.println(a);
            return null;
        }
    }
    /**
     * Prueft einen String auf moegliche Umlaute, die zu Fehlausgaben fuehren koennten. 
     * diese wurden vorher in einer HashMap zusammengefasst und auf Uebereinstimmungen durchsucht
     * @param str zu pruefender String
     * @return gibt die neue Schreibweise des Umlauts zurueck, oder NULL falls der str kein Umlaut war
     */
    private static String replaceUmlaute(String str){
            if (umlaute==null){
                umlaute = new HashMap<String,String>();
                umlaute.put("ä","ae");
                umlaute.put("ü","ue");
                umlaute.put("ö","oe");
                umlaute.put("ß","ss");
                umlaute.put("\u00DF","ss");
            }
            return umlaute.get(str); //**
    }
    
    
}
